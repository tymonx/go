// Copyright 2022 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package format

import (
	"errors"
	"strings"
)

// ErrTransform defines a custom transform error message function.
type ErrTransform func(err error) (message string)

// Err formats provided error(s) by unwrap and join.
func Err(errs ...error) string {
	return ErrMap(DefaultErrTransform, errs...)
}

// ErrMap formats provided error(s) by unwrap and join using map transform.
func ErrMap(transform ErrTransform, errs ...error) string {
	if transform == nil {
		transform = DefaultErrTransform
	}

	result := make([]string, 0, len(errs))

	for _, err := range errs {
		result = append(result, transform(err))

		for e := errors.Unwrap(err); e != nil; e = errors.Unwrap(e) {
			result = append(result, ErrMap(transform, e))
		}
	}

	return strings.Join(result, " ")
}

// DefaultErrTransform transform error message.
func DefaultErrTransform(err error) string {
	return err.Error()
}
