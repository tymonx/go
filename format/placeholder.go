// Copyright 2022 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package format

import (
	"fmt"
	"io"
)

type placeholder struct {
	used      map[int]bool
	arguments []interface{}
	length    int
	index     int
}

func newPlaceholder(arguments ...interface{}) *placeholder {
	return &placeholder{
		arguments: arguments,
		length:    len(arguments),
		used:      make(map[int]bool),
	}
}

func (p *placeholder) execute(position ...int) (value interface{}, err error) {
	index := p.index

	if len(position) != 0 {
		index = position[0]
	} else {
		p.index++
	}

	if index >= p.length {
		return nil, newError("argument placeholder index", index, "is out of bound", p.length, "arguments")
	}

	p.used[index] = true

	return p.arguments[index], nil
}

func (p *placeholder) write(message string, writer io.Writer) error {
	var space []byte

	if (message != "") && (message[len(message)-1] != ' ') {
		space = []byte{' '}
	}

	for index, argument := range p.arguments {
		if p.used[index] {
			continue
		}

		if _, err := writer.Write(space); err != nil {
			return newError("cannot write space separator at argument index", index).wrap(err)
		}

		space = []byte{' '}

		if _, err := fmt.Fprint(writer, argument); err != nil {
			return newError("cannot write argument", argument, "at index", index).wrap(err)
		}
	}

	return nil
}
