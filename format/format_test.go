// Copyright 2022 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package format_test

import (
	"bytes"
	"io"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/go/format"
	"gitlab.com/tymonx/go/format/mocks"
)

func TestFormatNew(test *testing.T) {
	assert.NotNil(test, format.New())
}

func TestFormatFormatOK(test *testing.T) {
	values := []struct {
		Delimiter   format.Delimiter
		Message     string
		Placeholder string
		Expected    string
		Arguments   []interface{}
	}{
		{
			Message:   "",
			Arguments: []interface{}{5, "arg1", true, false, 0.25, nil, struct{}{}},
			Expected:  "5 arg1 true false 0.25 <nil> {}",
		},
		{
			Message:   " ",
			Arguments: []interface{}{5, "arg1", true, false, 0.25, nil, struct{}{}},
			Expected:  " 5 arg1 true false 0.25 <nil> {}",
		},
		{
			Message:   "My message",
			Arguments: []interface{}{5, "arg1", true, false, 0.25, nil, struct{}{}},
			Expected:  "My message 5 arg1 true false 0.25 <nil> {}",
		},
		{
			Message:   "My message ",
			Arguments: []interface{}{5, "arg1", true, false, 0.25, nil, struct{}{}},
			Expected:  "My message 5 arg1 true false 0.25 <nil> {}",
		},
	}

	for index, value := range values {
		var buffer bytes.Buffer

		assert.NoError(test, format.New().
			SetDelimiters(value.Delimiter.Left, value.Delimiter.Right).
			SetPlaceholder(value.Placeholder).
			Format(&buffer, value.Message, value.Arguments...), "Index:", index)
		assert.Equal(test, value.Expected, buffer.String(), "Index:", index)
	}
}

func TestFormatFormatError(test *testing.T) {
	values := []struct {
		Delimiter   format.Delimiter
		Message     string
		Placeholder string
		Arguments   []interface{}
	}{
		{
			Message: "{p",
		},
		{
			Message:   "{p} {p}",
			Arguments: []interface{}{5},
		},
		{
			Message:     "{p}",
			Placeholder: "k",
			Arguments:   []interface{}{6},
		},
		{
			Message:   "<p}",
			Delimiter: format.Delimiter{Left: "<"},
		},
		{
			Message:   "{p>",
			Delimiter: format.Delimiter{Right: ">"},
		},
		{
			Message:   "<p>",
			Delimiter: format.Delimiter{Left: "<", Right: ">"},
		},
	}

	for index, value := range values {
		var buffer bytes.Buffer

		assert.Error(test, format.New().
			SetDelimiters(value.Delimiter.Left, value.Delimiter.Right).
			SetPlaceholder(value.Placeholder).
			Format(&buffer, value.Message, value.Arguments...), "Index:", index)
	}
}

func TestFormatFormatWriterSpaceError(test *testing.T) {
	controller := gomock.NewController(test)

	writer := mocks.NewMockWriter(controller)

	writer.EXPECT().Write(gomock.Any()).Return(0, io.ErrShortBuffer)

	assert.Error(test, format.New().Format(writer, "", 5))
}

func TestFormatFormatWriterArgumentError(test *testing.T) {
	controller := gomock.NewController(test)

	writer := mocks.NewMockWriter(controller)

	writer.EXPECT().Write(gomock.Any()).Return(0, nil)
	writer.EXPECT().Write(gomock.Any()).Return(0, io.ErrShortBuffer)

	assert.Error(test, format.New().Format(writer, "", 5))
}
