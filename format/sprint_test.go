// Copyright 2022 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package format_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/go/format"
)

func TestFormatSprint(test *testing.T) {
	values := []struct {
		Message   string
		Expected  string
		Arguments []interface{}
	}{
		{
			Message:  "",
			Expected: "",
		},
		{
			Message:  "My message",
			Expected: "My message",
		},
		{
			Message:  "Invalid {p",
			Expected: "cannot parse provided message: Invalid {p template: :1: unclosed action",
		},
	}

	for index, value := range values {
		assert.Equal(test, value.Expected, format.Sprint(value.Message, value.Arguments...), "Index:", index, "Message:", value.Message)
	}
}
