// Copyright 2022 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package format

import "fmt"

// Error defines a format error type.
type Error struct {
	err     error
	message string
}

// Error returns message error.
func (e *Error) Error() string {
	return e.message
}

// Unwrap unwraps wrapped error.
func (e *Error) Unwrap() error {
	return e.err
}

func newError(message string, arguments ...interface{}) *Error {
	if len(arguments) != 0 {
		message += " " + fmt.Sprint(arguments...)
	}

	return &Error{
		message: message,
	}
}

func (e *Error) wrap(err error) *Error {
	e.err = err

	return e
}
