// Copyright 2022 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package format

import (
	"io"
	"strings"
	"text/template"
)

// These constants defines default values for formatter.
const (
	DefaultPlaceholder    = "p"
	DefaultDelimiterLeft  = "{"
	DefaultDelimiterRight = "}"
)

var gFormat = Format{ //nolint: gochecknoglobals // thread-safe global cached format instance
	placeholder: DefaultPlaceholder,
	delimiter: Delimiter{
		Left:  DefaultDelimiterLeft,
		Right: DefaultDelimiterRight,
	},
}

// Format defines a format object type for formatting provide message.
type Format struct {
	placeholder string
	delimiter   Delimiter
}

// New creates a new format object.
func New() *Format {
	return &Format{
		placeholder: DefaultPlaceholder,
		delimiter: Delimiter{
			Left:  DefaultDelimiterLeft,
			Right: DefaultDelimiterRight,
		},
	}
}

// SetDelimiterLeft sets format left delimiter. Default is {.
func (f *Format) SetDelimiterLeft(delimiter string) *Format {
	if delimiter = strings.TrimSpace(delimiter); delimiter != "" {
		f.delimiter.Left = delimiter
	}

	return f
}

// SetDelimiterRight sets format right delimiter. Default is }.
func (f *Format) SetDelimiterRight(delimiter string) *Format {
	if delimiter = strings.TrimSpace(delimiter); delimiter != "" {
		f.delimiter.Right = delimiter
	}

	return f
}

// SetDelimiters sets format left and right delimiter. Default is {}.
func (f *Format) SetDelimiters(left, right string) *Format {
	return f.SetDelimiterLeft(left).SetDelimiterRight(right)
}

// SetPlaceholder sets format placeholder. Default is p.
func (f *Format) SetPlaceholder(placeholder string) *Format {
	if placeholder = strings.TrimSpace(placeholder); placeholder != "" {
		f.placeholder = placeholder
	}

	return f
}

// Format formats provided message and it writes to writer.
func (f *Format) Format(writer io.Writer, message string, arguments ...interface{}) (err error) {
	var data interface{}

	var instance *template.Template

	placeholder := newPlaceholder(arguments...)

	functions := template.FuncMap{
		f.placeholder: placeholder.execute,
	}

	if instance, err = template.New("").Delims(f.delimiter.Left, f.delimiter.Right).Funcs(functions).Parse(message); err != nil {
		return newError("cannot parse provided message:", message).wrap(err)
	}

	if err = instance.Execute(writer, data); err != nil {
		return newError("cannot format provided message:", message).wrap(err)
	}

	if err = placeholder.write(message, writer); err != nil {
		return newError("cannot write remaining arguments to provided message:", message).wrap(err)
	}

	return nil
}
