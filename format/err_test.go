// Copyright 2022 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package format_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/go/format"
)

type Error struct {
	err     error
	message string
}

func NewError(message string) *Error {
	return &Error{message: message}
}

func (e *Error) Error() string {
	return e.message
}

func (e *Error) Unwrap() error {
	return e.err
}

func (e *Error) Wrap(err error) error {
	e.err = err

	return e
}

func TestFormatErr(test *testing.T) {
	values := []struct {
		Expected string
		Errs     []error
	}{
		{
			Expected: "",
		},
		{
			Expected: "my error A",
			Errs: []error{
				NewError("my error A"),
			},
		},
		{
			Expected: "my error A my error B",
			Errs: []error{
				NewError("my error A").Wrap(NewError("my error B")),
			},
		},
		{
			Expected: "my error A my error B my error C",
			Errs: []error{
				NewError("my error A").Wrap(NewError("my error B")),
				NewError("my error C"),
			},
		},
	}

	for index, value := range values {
		assert.Equal(test, value.Expected, format.Err(value.Errs...), "Index:", index)
	}
}

func TestFormatErrMap(test *testing.T) {
	values := []struct {
		Transform format.ErrTransform
		Expected  string
		Errs      []error
	}{
		{
			Transform: nil,
			Expected:  "",
		},
		{
			Transform: nil,
			Expected:  "my error A",
			Errs: []error{
				NewError("my error A"),
			},
		},
		{
			Transform: nil,
			Expected:  "my error A my error B",
			Errs: []error{
				NewError("my error A").Wrap(NewError("my error B")),
			},
		},
		{
			Transform: nil,
			Expected:  "my error A my error B my error C",
			Errs: []error{
				NewError("my error A").Wrap(NewError("my error B")),
				NewError("my error C"),
			},
		},
		{
			Transform: func(err error) string {
				return err.Error() + "."
			},
			Expected: "",
		},
		{
			Transform: func(err error) string {
				return err.Error() + "."
			},
			Expected: "my error A.",
			Errs: []error{
				NewError("my error A"),
			},
		},
		{
			Transform: func(err error) string {
				return err.Error() + "."
			},
			Expected: "my error A. my error B.",
			Errs: []error{
				NewError("my error A").Wrap(NewError("my error B")),
			},
		},
		{
			Transform: func(err error) string {
				return err.Error() + "."
			},
			Expected: "my error A. my error B. my error C.",
			Errs: []error{
				NewError("my error A").Wrap(NewError("my error B")),
				NewError("my error C"),
			},
		},
	}

	for index, value := range values {
		assert.Equal(test, value.Expected, format.ErrMap(value.Transform, value.Errs...), "Index:", index)
	}
}
